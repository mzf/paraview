# Expose Tone Mapping and SSAO in render view

Tone mapping is useful to display correctly HDR images and is configurable in the
render view settings panel. SSAO allows to take in account ambient occlusion to
create shadows made by the object itself.
You can configure the tone mapping and SSAO in the render view settings and you can enable or
disable them in each render view under the property panel.
